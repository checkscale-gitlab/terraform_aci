/*
    Copyright (c) 2020 World Wide Technology, LLC
    All rights reserved.
    
    author: Joel W. King  @joelwking

    usage:

      Example of using the local provider to create a file and execute Ansible. Normally you would want to
      define the provisioner under the resource you wish to provison using Ansible. In this example, the local provider
      may execute before or after the resources specified in the `resources.tf` file.

      Destroy will delete the file specified.

      You can also run Ansible as a provisioner without a resource, using the null_resource.

    references:
      - https://github.com/scarolan/ansible-terraform
      - https://registry.terraform.io/providers/hashicorp/local/latest/docs
      - https://github.com/sethvargo/terraform-provider-filesystem
      - https://www.terraform.io/docs/language/resources/provisioners/null_resource.html

*/  

resource "local_file" "foo" {
    content     = "foo!"
    filename = "/tmp/foo/bar"

    # filename = "${path.module}/foo.bar"

  provisioner "local-exec" {
    command = "ansible-playbook --version"
  }
}

/*
    Provisioners Without a Resource, run Ansible ad-hoc command under a null_resource 
*/

resource "null_resource" "uptime" {
  provisioner "local-exec" {
    command = "ansible localhost -m shell -a uptime"
  }
}