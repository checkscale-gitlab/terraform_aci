/*
    Copyright (c) 2020 World Wide Technology, LLC
    All rights reserved.
    
    author: Joel W. King  @joelwking

    usage:
 
      To declare variables use the variable block in one of your .tf files, such as `variables.tf`.

      This file is module.lldp variables.tf

    references:
      - https://www.terraform.io/docs/configuration/variables.html

*/

variable "lldp_description" {
  description = "LLDP Description"
  default = " "
  type = string
}

variable "lldp_name" {
  description = "LLDP policy name"
  default = "lldp_policy"
  type = string
}

variable "lldp_admin_rx_st" {
  description = "LLDP Receive State"
  default = "enabled"
  type = string
}

variable "lldp_admin_tx_st" {
  description = "LLDP Transmit State"
  default = "enabled"
  type = string
}

variable "lldp_annotation" {
  description = "LLDP Annotation"
  default = "tag_lldp"
  type = string
}

variable "lldp_name_alias" {
  description = "LLDP Alias"
  default = "alias_lldp"
  type = string
}
