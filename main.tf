/*
    Copyright (c) 2020 World Wide Technology, LLC
    All rights reserved.
    
    author: Joel W. King  @joelwking

    usage:

      This is the root module for the Terraform ACI demonstration. The root module contains the resources to manage 
      aspects of the ACI fabric. In this directory, we have segregated the various aspects of a Terraform configuration
      in separate files, rather than including all definitions in the `main.tf`, of the main working directory, to 
      illustrate each component. The root module configuration is defined in:


          ├── backend.tf
          ├── credentials.auto.tfvars
          ├── locals.tf
          ├── main.tf                           <- You are here
          ├── modules
          │   └── lldp
          ├── outputs.tf
          ├── providers.tf
          ├── README.md
          ├── resources.tf
          ├── variables.tf
          └── versions.tf


      A module is a container for multiple resources that are used together. Every Terraform configuration has at least one module, known as its root module, which consists of the resources defined in the . tf files in the main working directory.

      Within the main working directory, there is a subdirectory named `modules`, which contains a submodule to manage the LLDP
      configuration. We are creating multiple instance of this module, through the `for_each` expression. We iterate over each of
      the LLDP configuration specified in the `variables.tf` file variable reference `lldp`.

      In this example, the source of the sub-module is the local file system, but could also be sourced from GitHub, AWS S3, etc.

    references:
      - https://www.terraform.io/docs/configuration/modules.html#multiple-instances-of-a-module
      - https://www.terraform.io/docs/modules/sources.html

*/

module "lldp" {
  source                = "./modules/lldp"

  for_each = var.lldp
      lldp_name         =  each.value.name
      lldp_description  =  "${local.service_request} by ${local.initiator}"
      lldp_admin_rx_st  =  each.value.admin_rx_st
      lldp_admin_tx_st  =  each.value.admin_tx_st
}