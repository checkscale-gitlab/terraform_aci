/*
    Copyright (c) 2020 World Wide Technology, LLC
    All rights reserved.
    
    author: Joel W. King  @joelwking

    usage:

      Output values are the return values of a Terraform module or submodule

    references:
      - https://www.terraform.io/docs/configuration/outputs.html

*/
#
#  These outputs print values from the `main.tf` root module after issuing `terraform apply`. 
#
output "ACI_TENANT_BROWNFOX_ID" {
  value = aci_tenant.BROWNFOX.id
  description = "The ID of the BROWNFOX tenant"
}
output "ACI_TENANT_BROWNFOX_DESCRIPTION" {
  value = aci_tenant.BROWNFOX.description
  description = "The description of the BROWNFOX tenant"
}
output "VRF_NAME" {
  value = aci_vrf.GREEN.name
  description = "The name of the GREEN VRF"
}
output "VRF_ID" {
  value = aci_vrf.GREEN.id
  description = "The ID of the GREEN VRF"
}
#
#  Data Source output, refer to file data_sources.tf
#
output "ACI_USER_ACCOUNT_STATUS" {
  value = data.aci_local_user.ACI_DATASOURCE.account_status
  description = "Account status of the ACI user"
}
#
#  These outputs reference (print) variables for instances of the submodule(s) lldp
#
output "Secondary" {
  value = module.lldp["secondary"].LLDP_ID
}

output "Primary" {
  value = module.lldp["primary"].LLDP_ID
}